<?php

/**
 * @file
 *
 * Provide views data and handlers for image_upload tables.
 */

/**
 * @defgroup views_image_upload_module image_upload.module handlers
 *
 * @{
 */

/**
 * Implementation of hook_views_data()
 */
function image_upload_views_data() {
  $data = array();

  // ----------------------------------------------------------------------
  // image_upload table

  $data['image_upload']['table']['group']  = t('Image Upload');

  $data['image_upload']['table']['join'] = array(
    'node' => array(
      'left_field' => 'vid',
      'field' => 'vid',
    ),
    'node_revisions' => array(
      'left_field' => 'vid',
      'field' => 'vid',
    ),
    'files' => array(
      'left_field' => 'fid',
      'field' => 'fid',
    ),
  );

  $data['image_upload']['vid'] = array(
    'title' => t('Node'),
    'help' => t('The node the uploaded image is attached to'),
     'relationship' => array(
      'label' => t('image_upload'),
      'base' => 'node',
      'base field' => 'vid',
      // This allows us to not show this relationship if the base is already
      // node so users won't create circular relationships.
      'skip base' => 'node',
    ),
  );

  $data['image_upload']['description'] = array(
    'title' => t('Description'),
    'help' => t('The description of the uploaded image.'),
    'field' => array(
      'handler' => 'views_handler_field_image_upload_description',
      'click sortable' => TRUE,
     ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['image_upload']['weight'] = array(
    'title' => t('Weight'),
    'help' => t('The weight, used for sorting.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
     ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  return $data;
}

function image_upload_views_data_alter(&$data) {
  $data['node']['image_upload_fid'] = array(
    'group' => t('Image Upload'),
    'title' => t('Attached images'),
    'help' => t('All images attached to a node with image_upload.module.'),
    'real field' => 'vid',
    'field' => array(
      'handler' => 'views_handler_field_image_upload_fid',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_image_upload_fid',
      'title' => t('Has attached images'),
      'help' => t('Only display items with attached images. This can cause duplicates if there are multiple attached images.'),
    ),
    'relationship' => array(
      'title' => t('Attached images'),
      'help' => t('Add a relationship to gain access to more file data for images uploaded by image_upload.module. Note that this relationship will cause duplicate nodes if there are multiple images attached to the node.'),
      'relationship table' => 'image_upload',
      'relationship field' => 'fid',
      'base' => 'files',
      'field' => 'fid',
      'handler' => 'views_handler_relationship',
      'label' => t('Images'),
    ),
  );
}

/**
 * Field handler to provide a list of roles.
 */
class views_handler_field_image_upload_fid extends views_handler_field_prerender_list {
  function construct() {
    parent::construct();
  }
  
  function get_terms() {
    $terms = array();
    
    $result = db_query("SELECT vid FROM {vocabulary} WHERE name = 'Image Upload'");
    while ($vocabulary = db_fetch_object($result)) {
      $vid = $vocabulary->vid;
    }
    
    $result = db_query("SELECT name, tid FROM {term_data} WHERE vid = %d", $vid);
    
    while ($term = db_fetch_object($result)) {
      $terms[$term->tid] = $term->name;
    }
    
    return $terms;
  }
  
  function query() {
    parent::query();
    
    $this->ensure_my_table();
    
    $this->query->add_groupby("node.vid");

    if(count($this->options['link_to_term']) > 0) {
      $tids = array();
      foreach($this->options['link_to_term'] as $tid) {
        $tids[] = $tid;
      }
      
      $tids = implode(",", $tids);
      
      $this->query->add_where($this->options['group'], "image_upload.tid IN($tids)");
    }
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['link_to_file'] = array('default' => FALSE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['link_to_file'] = array(
      '#title' => t('Link this field to the full image.'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_to_file']),
    );
    
    $form['only_one'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show only the first uploaded image.'),
      '#default_value' => $this->options['only_one'],
    );
    
    $form['link_to_term'] = array(
      '#type' => 'select',
      '#title' => t('Only show the images from these categories:'),
      '#default_value' => $this->options['link_to_term'],
      '#options' => $this->get_terms(),
      '#multiple' => TRUE,
    );
  }
  
  function pre_render($values) {
    $vids = array();
    $this->items = array();
    
    $data = array();
    foreach ($values as $result) {
      $vids[] = $result->{$this->field_alias};
    }

    if ($vids) {
      $vids = array_unique($vids);
      
      $group_by = '';
      
      if(!empty($this->options['only_one'])) {
        $group_by = ' GROUP BY u.vid ';  
      }
      
      $result = db_query("SELECT u.vid, u.fid, f.filepath, u.thumbpath, u.description FROM {image_upload} u LEFT JOIN {files} f ON f.fid = u.fid WHERE u.vid IN (" . implode(', ', $vids) . ") $group_by ORDER BY u.weight ASC, u.fid ASC");
   
      while ($file = db_fetch_object($result)) {
        $this->items[$file->vid][$file->fid] = $this->render_link($file->thumbpath, $file);
      }
    }
  }

  /**
   * Render whatever the data is as a link to the file.
   *
   * Data should be made XSS safe prior to calling this function.
   */
  function render_link($data, $values) {
    list($width, $height, $type, $image_attributes) = @getimagesize($values->thumbpath);
  
    $data = '<img src="' . url($data, array('absolute' => true))  .'" width="' . $width . '" height="' . $height . '" />';
    
    if (!empty($this->options['link_to_file'])) {      
      $options = array(
        'html' => TRUE,
        'attributes' => array('rel' => 'lightbox[view][' . $values->description . ']')
      );
      
      return l($data, file_create_url($values->filepath), $options);
    }
    else {
      return $data;
    }
  }
}

/**
 * Field handler to provide a list of roles.
 */
class views_handler_field_image_upload_description extends views_handler_field_prerender_list {
  function init(&$view, &$options) {
    parent::init($view, $options);
    if (!empty($options['link_to_file'])) {
      $this->additional_fields['fid'] = 'fid';
    }
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['link_to_file'] = array('default' => FALSE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['link_to_file'] = array(
      '#title' => t('Link this field to the full image.'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_to_file']),
    );
  }

  function pre_render($values) {
    if (empty($this->options['link_to_file'])) {
      return;
    }

    $fids = array();
    $this->items = array();

    $data = array();
    foreach ($values as $result) {
      if ($result->{$this->aliases['fid']}) {
        $fids[] = $result->{$this->aliases['fid']};
      }
    }

    if ($fids) {
      $result = db_query("SELECT f.fid, f.filepath FROM {files} f WHERE f.fid IN (" . implode(', ', $fids) . ")");
      while ($file = db_fetch_object($result)) {
        $this->items[$file->fid] = $file;
      }
    }
  }

  function render($values) {
    return $this->render_link($values->{$this->field_alias}, $values);
  }

  /**
   * Render whatever the data is as a link to the file.
   *
   * Data should be made XSS safe prior to calling this function.
   */
  function render_link($data, $value) {
    if (!empty($this->options['link_to_file']) && $value->{$this->aliases['fid']}) {
      $values = $this->items[$value->{$this->aliases['fid']}];
      return l($data, file_create_url($values->filepath), array('html' => TRUE));
    }
    else {
      return $data;
    }
  }
}

/**
 * Filter by whether or not a node has attached images from the image_upload module
 */
class views_handler_filter_image_upload_fid extends views_handler_filter_boolean_operator {
  function construct() {
    parent::construct();
    $this->value_value = t('Has attached images');
  }

  function query() {
    $this->ensure_my_table();
    $this->query->add_where($this->options['group'], "(SELECT COUNT(*) FROM {image_upload} u WHERE u.vid = $this->table_alias.$this->real_field) " . (empty($this->value) ? '=' : '<>') . " 0");
  }
}


/**
 * @}
 */
